// This is the service worker with the Cache-first network

const CACHE = "pwabuilder-precache";
const precacheFiles = [
'../favicon.ico',
'../css/animate.css',
'../css/animate.min.css',
'../css/back.png',
'../css/bootstrap.css',
'../css/bootstrap.min.css',
'../css/demo.css',
'../css/font-awesome.min.css',
'../css/style.css',
'../css/fancybox/jquery.fancybox-buttons.css',
'../css/fancybox/jquery.fancybox-thumbs.css',
'../css/fancybox/jquery.fancybox.css',
'../fancybox-master/dist/jquery.fancybox.css',
'../fancybox-master/dist/jquery.fancybox.js',
'../fancybox-master/dist/jquery.fancybox.min.css',
'../fancybox-master/dist/jquery.fancybox.min.js',
'../fancybox-master/docs/index.html',
'../fancybox-master/src/css/core.css',
'../fancybox-master/src/css/fullscreen.css',
'../fancybox-master/src/css/share.css',
'../fancybox-master/src/css/slideshow.css',
'../fancybox-master/src/css/thumbs.css',
'../fancybox-master/src/js/core.js',
'../fancybox-master/src/js/fullscreen.js',
'../fancybox-master/src/js/guestures.js',
'../fancybox-master/src/js/hash.js',
'../fancybox-master/src/js/media.js',
'../fancybox-master/src/js/share.js',
'../fancybox-master/src/js/slideshow.js',
'../fancybox-master/src/js/thumbs.js',
'../fancybox-master/src/js/wheel.js',
'../font-awesome/css/font-awesome.css',
'../font-awesome/css/font-awesome.min.css',
'../font-awesome/fonts/fontawesome-webfont.eot',
'../font-awesome/fonts/fontawesome-webfont.svg',
'../font-awesome/fonts/fontawesome-webfont.ttf',
'../font-awesome/fonts/fontawesome-webfont.woff',
'../font-awesome/fonts/fontawesome-webfont.woff2',
'../font-awesome/fonts/FontAwesome.otf',
'../font-awesome/less/animated.less',
'../font-awesome/less/bordered-pulled.less',
'../font-awesome/less/core.less',
'../font-awesome/less/fixed-width.less',
'../font-awesome/less/font-awesome.less',
'../font-awesome/less/icons.less',
'../font-awesome/less/larger.less',
'../font-awesome/less/list.less',
'../font-awesome/less/mixins.less',
'../font-awesome/less/path.less',
'../font-awesome/less/rotated-flipped.less',
'../font-awesome/less/screen-reader.less',
'../font-awesome/less/spinning.less',
'../font-awesome/less/stacked.less',
'../font-awesome/less/variables.less',
'../font-awesome/scss/font-awesome.scss',
'../font-awesome/scss/_animated.scss',
'../font-awesome/scss/_bordered-pulled.scss',
'../font-awesome/scss/_core.scss',
'../font-awesome/scss/_fixed-width.scss',
'../font-awesome/scss/_icons.scss',
'../font-awesome/scss/_larger.scss',
'../font-awesome/scss/_list.scss',
'../font-awesome/scss/_mixins.scss',
'../font-awesome/scss/_path.scss',
'../font-awesome/scss/_rotated-flipped.scss',
'../font-awesome/scss/_screen-reader.scss',
'../font-awesome/scss/_spinning.scss',
'../font-awesome/scss/_stacked.scss',
'../font-awesome/scss/_variables.scss',
'../images/fancybox/blank.gif',
'../images/fancybox/fancybox_buttons.png',
'../images/fancybox/fancybox_loading.gif',
'../images/fancybox/fancybox_sprite.png',
'../img/abpi.svg',
'../img/glenmark-logo.png',
'../img/home.svg',
'../img/next.png',
'../img/next.svg',
'../img/page3.svg',
'../img/play-button.png',
'../img/prev.png',
'../img/prev.svg',
'../img/ref.png',
'../img/ref.svg',
'../img/oncology/abpi.svg',
'../img/oncology/capsule.svg',
'../img/oncology/home.png',
'../img/oncology/home.svg',
"../img/oncology/abirapro/page1/bg.png",
"../img/oncology/abirapro/page2/bg.png",
"../img/oncology/abirapro/page3/bg.png",
"../img/oncology/abirapro/page4/bg.png",

'../img/oncology/aprecap/page1/bg.png',
'../img/oncology/aprecap/page2/bg.png',
'../img/oncology/aprecap/page2/img1.png',
'../img/oncology/aprecap/page2/img3.png',
'../img/oncology/aprecap/page2/logo.png',
'../img/oncology/aprecap/page2/ref-text.png',
'../img/oncology/aprecap/page2/text.png',
'../img/oncology/aprecap/page3/packshot.png',
'../img/oncology/aprecap/page3/ref-text.png',
'../img/oncology/aprecap/page3/text.png',
'../img/oncology/aprecapiv/page1/bg.png',
'../img/oncology/aprecapiv/page2/bg.png',
'../img/oncology/aprecapiv/page2/packshot.png',
'../img/oncology/aprecapiv/page2/ref-text.png',
'../img/oncology/aprecapiv/page2/text.png',

'../img/oncology/azactiv/page1/bg.png',
'../img/oncology/azactiv/page2/bg.png',
'../img/oncology/azactiv/page2/grapg.png',
'../img/oncology/azactiv/page2/big-graph.png',
'../img/oncology/azactiv/page3/bg.png',

'../img/oncology/bevicra/page1/bg.png',
'../img/oncology/bevicra/page2/bg.png',
'../img/oncology/bevicra/page2/logo.png',
'../img/oncology/bevicra/page2/packshot.png',
'../img/oncology/bevicra/page2/text.png',
'../img/oncology/bortrac/page1/bg.png',
'../img/oncology/bortrac/page1/graph1-big.png',
'../img/oncology/bortrac/page1/graph1.png',
'../img/oncology/bortrac/page1/graph2-big.png',
'../img/oncology/bortrac/page1/graph2.png',
'../img/oncology/bortrac/page1/ref-text.png',
'../img/oncology/bortrac/page1/text.png',
'../img/oncology/bortrac/page2/graph-big.png',
'../img/oncology/bortrac/page2/graph.png',
'../img/oncology/bortrac/page2/ref-text.png',
'../img/oncology/bosuvi/page1/bg.png',
'../img/oncology/bosuvi/page2/bg.png',
'../img/oncology/bosuvi/page2/logo.png',
'../img/oncology/bosuvi/page2/packshot.png',
'../img/oncology/bosuvi/page3/img.png',
'../img/oncology/bosuvi/page3/ref-text.png',
'../img/oncology/bosuvi/page4/graph-big.png',
'../img/oncology/bosuvi/page4/graph.png',
'../img/oncology/bosuvi/page4/img1.png',
'../img/oncology/bosuvi/page4/img2.png',
'../img/oncology/bosuvi/page4/ref-text.png',
'../img/oncology/bosuvi/page4/text1.png',
'../img/oncology/bosuvi/page4/text2.png',
'../img/oncology/bosuvi/page5/img.png',
'../img/oncology/bosuvi/page5/logo.png',

'../img/oncology/sutib/page1/bg.png',
'../img/oncology/sutib/page2/bg.png',
'../img/oncology/sutib/page3/bg.png',
'../img/oncology/sutib/page3/graph.png',
'../img/oncology/sutib/page4/bg.png',
'../img/oncology/sutib/page4/video.mp4',

'../img/oncology/axzyb/page1/bg.png',
'../img/oncology/axzyb/page2/bg.png',
'../img/oncology/axzyb/page3/bg.png',
'../img/oncology/axzyb/page4/bg.png',
'../img/oncology/axzyb/page5/bg.png',
'../img/oncology/brand-logo/abirapro-logo.png',
'../img/oncology/brand-logo/aprecap-iv-logo.png',
'../img/oncology/brand-logo/aprecap-logo.png',
'../img/oncology/brand-logo/azactiv.png',
'../img/oncology/brand-logo/bevicra.png',
'../img/oncology/brand-logo/bortrac-logo.png',
'../img/oncology/brand-logo/bosuvi.png',
'../img/oncology/brand-logo/deczuba-logo.png',
'../img/oncology/brand-logo/deghor.png',
'../img/oncology/brand-logo/epithra-logo.png',
'../img/oncology/brand-logo/erleva-logo.png',
'../img/oncology/brand-logo/evermil-logo.png',
'../img/oncology/brand-logo/fulviglen-logo.png',
"../img/oncology/fulviglen/page1/bg.png",
"../img/oncology/fulviglen/page2/bg.png",
"../img/oncology/fulviglen/page2/graph1.png",
"../img/oncology/fulviglen/page2/graph2.png",
"../img/oncology/fulviglen/page2/graph3.png",
"../img/oncology/fulviglen/page2/big-graph1.png",
"../img/oncology/fulviglen/page2/big-graph2.png",
"../img/oncology/fulviglen/page2/big-graph3.png",
"../img/oncology/fulviglen/page3/bg.png",
"../img/oncology/fulviglen/page3/graph1.png",
"../img/oncology/fulviglen/page3/graph2.png",
"../img/oncology/fulviglen/page4/bg.png",
"../img/oncology/fulviglen/page4/graph1.png",
"../img/oncology/fulviglen/page4/big-graph1.png",
"../img/oncology/fulviglen/page5/bg.png",
"../img/oncology/fulviglen/page6/bg.png",
'../img/oncology/brand-logo/geftib-logo.png',
'../img/oncology/brand-logo/glenstin-peg-logo.png',
'../img/oncology/brand-logo/glenza-logo.png',
'../img/oncology/brand-logo/glerelin.png',
'../img/oncology/brand-logo/lapahope.png',
'../img/oncology/brand-logo/mitinab-logo.png',
'../img/oncology/brand-logo/palnox.png',
'../img/oncology/brand-logo/pexotra-logo.png',
'../img/oncology/brand-logo/pomacel.png',
'../img/oncology/brand-logo/procabazi.png',
'../img/oncology/brand-logo/trumab-logo.png',
'../img/oncology/brand-logo/sutib-logo.png',
'../img/oncology/brand-logo/Axyb-logo.png',
'../img/oncology/deczuba/page1/bg.png',
'../img/oncology/akynzeo/akynzeo/page6/bg.png',
'../img/oncology/akynzeo/akynzeo/page5/bg.png',
'../img/oncology/akynzeo/akynzeo/page4/bg.gif',
'../img/oncology/akynzeo/akynzeo/page3/bg.gif',
'../img/oncology/akynzeo/akynzeo/page2/bg.gif',
'../img/oncology/akynzeo/akynzeo/page1/bg.png',
'../img/oncology/akynzeo/page1/bg1.png',
'../img/oncology/akynzeo/page1/b.png',
'../img/oncology/akynzeo/page1/b1.png',
'../img/oncology/akynzeo/page1/b3.png',
'../img/oncology/akynzeo/akynzeo/page7/bg.png',
'../img/oncology/akynzeo/akynzeo/page8/bg.png',
'../img/oncology/akynzeo/akynzeo/page9/bg.png',
'../img/oncology/akynzeo/akynzeo/page10/bg.png',
'../img/oncology/akynzeo/akynzeo/page11/bg.png',
'../img/oncology/akynzeo/akynzeo/page12/bg.png',

'../img/oncology/deczuba/page1/bg.png',
'../img/oncology/deczuba/page2/bg.png',
'../img/oncology/deczuba/page3/bg.png',

'../img/oncology/deghor/page1/bg.png',
'../img/oncology/deghor/page2/bg.png',
'../img/oncology/deghor/page2/logo.png',
'../img/oncology/deghor/page2/packshot.png',
'../img/oncology/deghor/page2/text.png',
'../img/oncology/deghor/page3/img.png',
'../img/oncology/deghor/page3/ref-text.png',
'../img/oncology/deghor/page3/text1.png',
'../img/oncology/deghor/page3/text2.png',
'../img/oncology/deghor/page4/graph1-big.png',
'../img/oncology/deghor/page4/graph1.png',
'../img/oncology/deghor/page4/graph2-big.png',
'../img/oncology/deghor/page4/graph2.png',
'../img/oncology/deghor/page4/ref-text.png',
'../img/oncology/deghor/page4/text.png',
'../img/oncology/epithra/page1/bg.png',
'../img/oncology/epithra/page1/logo.png',
'../img/oncology/epithra/page1/text.png',
'../img/oncology/erleva/page1/bg.png',
'../img/oncology/erleva/page1/logo.png',
'../img/oncology/erleva/page1/packshot.png',
'../img/oncology/erleva/page1/ref-text.png',
'../img/oncology/erleva/page1/text.png',
'../img/oncology/evermil/page1/bg.png',
'../img/oncology/evermil/page2/bg.png',
'../img/oncology/evermil/page2/img1.png',
'../img/oncology/evermil/page2/img2.png',
'../img/oncology/evermil/page2/img3.png',
'../img/oncology/evermil/page2/text.png',
'../img/oncology/evermil/page3/img.png',
'../img/oncology/evermil/page3/text.png',
'../img/oncology/evermil/page4/logo.png',
'../img/oncology/evermil/page4/packshot.png',
'../img/oncology/evermil/page4/text.png',
'../img/oncology/fulviglen/page1/bg.png',
'../img/oncology/fulviglen/page2/bg.png',
'../img/oncology/fulviglen/page2/packshot.png',
'../img/oncology/fulviglen/page2/ref-text.png',
'../img/oncology/fulviglen/page2/text.png',
'../img/oncology/geftib/page1/bg.png',
'../img/oncology/geftib/page1/img1.png',
'../img/oncology/geftib/page1/img2.png',
'../img/oncology/geftib/page1/logo.png',
'../img/oncology/geftib/page1/ref-text.png',
'../img/oncology/glenstimpeg/page1/bg.png',
'../img/oncology/glenstimpeg/page2/bg.png',
'../img/oncology/glenstimpeg/page3/bg.png',
'../img/oncology/glenza/page1/img.png',
'../img/oncology/glenza/page2/bg.png',
'../img/oncology/glenza/page2/img.png',
'../img/oncology/glenza/page2/ref-text.png',
'../img/oncology/glenza/page2/text1.png',
'../img/oncology/glenza/page3/img.png',
'../img/oncology/glenza/page3/text.png',
'../img/oncology/glenza/page4/bg.png',
'../img/oncology/glenza/page4/img.png',
'../img/oncology/glenza/page4/ref-text.png',
'../img/oncology/glenza/page4/text.png',
'../img/oncology/glenza/page5/bg.png',
'../img/oncology/glenza/page5/img.png',
'../img/oncology/glenza/page5/ref-text.png',
'../img/oncology/glenza/page5/text.png',
'../img/oncology/glenza/page6/bg.png',
'../img/oncology/glenza/page6/img.png',
'../img/oncology/glenza/page6/ref-text.png',
'../img/oncology/glenza/page6/text.png',
'../img/oncology/glenza/page7/img.png',
'../img/oncology/glenza/page7/ref-text.png',
'../img/oncology/glenza/page7/text.png',

'../img/oncology/glerelin/page1/bg.png',
'../img/oncology/glerelin/page2/bg.png',
'../img/oncology/glerelin/page3/bg.png',
'../img/oncology/glerelin/page3/graph.png',
'../img/oncology/glerelin/page4/bg.png',
'../img/oncology/glerelin/page4/graph.png',

'../img/oncology/lapahope/page1/bg.png',
'../img/oncology/lapahope/page2/bg.png',
'../img/oncology/lapahope/page2/img.png',
'../img/oncology/lapahope/page2/logo.png',
'../img/oncology/mitinab/page1/bg.png',
'../img/oncology/mitinab/page2/bg.png',
'../img/oncology/mitinab/page2/img1.png',
'../img/oncology/mitinab/page2/img2.png',
'../img/oncology/mitinab/page2/logo.png',
'../img/oncology/mitinab/page2/ref-text.png',
'../img/oncology/page1/bg.png',
'../img/oncology/page1/logo.png',
'../img/oncology/palnox/page1',
'../img/oncology/palnox/page2',
'../img/oncology/palnox/page1/bg.png',
'../img/oncology/palnox/page2/bg.png',
'../img/oncology/pexotra/page1/bg.png',
'../img/oncology/pexotra/page1/img1.png',
'../img/oncology/pexotra/page1/img2.png',
'../img/oncology/pexotra/page1/logo.png',
'../img/oncology/pexotra/page1/ref-text.png',

'../img/oncology/pomacel/bg1.png',
'../img/oncology/pomacel/bg2.png',
'../img/oncology/pomacel/bg3.png',
'../img/oncology/procabazi/page1/bg.png',
'../img/oncology/procabazi/page2/bg.png',
'../img/oncology/procabazi/page2/graph1-big.png',
'../img/oncology/procabazi/page2/graph1.png',
'../img/oncology/procabazi/page2/graph2-big.png',
'../img/oncology/procabazi/page2/graph2.png',
'../img/oncology/procabazi/page2/ref-text.png',
'../img/oncology/procabazi/page2/text.png',
'../img/oncology/procabazi/page2/text2.png',
'../img/oncology/trumab/page1/bg.png',
'../img/oncology/trumab/page2/bg.png',
'../img/oncology/trumab/page2/img.png',
'../img/oncology/trumab/page2/logo.png',
'../img/oncology/trumab/page2/packshot.png',
'../img/oncology/trumab/page2/ref-text.png',
'../img/oncology/trumab/page2/text.png',
"../img/darbilen/bg1.png",
"../img/darbilen/bg2.png",
"../img/darbilen/bg3.png",


"../img/mistaurin/page1/bg.png",
"../img/mistaurin/page2/bg.png",
"../img/mistaurin/page3/bg.png",
"../img/mistaurin/page3/graph1.png",
"../img/mistaurin/page3/graph2.png",
"../img/mistaurin/page3/graph3.png",
"../img/mistaurin/page4/bg.png",

'../img/oncology/pazib/page1/bg.png',
'../img/oncology/pazib/page1/img.png',
'../img/oncology/pazib/page2/bg.png',
'../img/oncology/pazib/page2/img1.png',
'../img/oncology/pazib/page2/img2.png',
'../img/oncology/pazib/page3/bg.png',
'../img/oncology/pazib/page4/bg.png',
'../img/oncology/pazib/page5/img1.png',
'../img/oncology/pazib/page5/img2.png',
'../img/oncology/pazib/page5/bg.png',

'../img/oncology/pazib/page6/bg.png',
'../img/oncology/pazib/page7/bg.png',
'../img/oncology/pazib/page7/link1.png',
'../img/oncology/pazib/page7/link2.png',
'../img/oncology/pazib/page7/link3.png',
'../img/oncology/pazib/page7/link4.png',
'../img/oncology/pazib/page7/link5.png',
'../img/oncology/pazib/page7/link6.png',
'../img/oncology/pazib/page7/link7.png',
'../img/oncology/pazib/page8/bg.png',
'../img/oncology/pazib/page9/bg.png',
'../img/oncology/pazib/page10/bg.png',
'../img/oncology/pazib/page11/bg.png',
'../img/oncology/pazib/page26/bg.png',
'../img/oncology/pazib/page13/img1.png',
'../img/oncology/pazib/page13/img2.png',
'../img/oncology/pazib/page13/img3.png',
'../img/oncology/pazib/page13/bg.png',
'../img/oncology/pazib/page14/bg.png',
'../img/oncology/pazib/page15/small-graph.png',
'../img/oncology/pazib/page15/graph.png',
'../img/oncology/pazib/page15/bg.png',
'../img/oncology/pazib/page16/graph1.png',
'../img/oncology/pazib/page16/s-graph.png',
'../img/oncology/pazib/page16/graph2.png',
'../img/oncology/pazib/page16/s-graph2.png',
'../img/oncology/pazib/page16/bg.png',
'../img/oncology/pazib/page17/bg.png',
'../img/oncology/pazib/page20/bg.png',
'../img/oncology/pazib/page18/bg.png',
'../img/oncology/pazib/page19/graph1.png',
'../img/oncology/pazib/page19/graph2.png',
'../img/oncology/pazib/page19/bg.png',
'../img/oncology/pazib/page21/bg.png',
'../img/oncology/pazib/page22/bg.png',
'../img/oncology/pazib/page23/bg.png',
'../img/oncology/pazib/page24/bg.png',
'../img/oncology/pazib/page25/bg.png',
'../img/oncology/pazib/page26/bg.png',
'../img/oncology/abpi/1.png',
'../img/oncology/abpi/2.png',
'../img/oncology/abpi/3.png',
'../img/oncology/abpi/4.png',
'../img/oncology/abpi/5.png',
'../img/oncology/abpi/6.png',
'../img/oncology/abpi/7.png',
'../img/oncology/abpi/8.png',
'../img/oncology/abpi/9.png',
'../img/oncology/abpi/10.png',
'../img/oncology/abpi/11.png',
'../img/oncology/abpi/12.png',
'../img/oncology/abpi/13.png',
'../img/oncology/abpi/14.png',

'../js/bootstrap.min.js',
'../js/jquery-2.1.1.min.js',
'../js/jquery-3.3.1.min.js',
'../js/jquery.min.js',
'../js/popper.min.js',
'../js/preference.js',
'../js/swip.min.js',
'../js/switcher.js',
'../js/fancybox/jquery.easing-1.3.pack.js',
'../js/fancybox/jquery.fancybox-buttons.js',
'../js/fancybox/jquery.fancybox-thumbs.js',
'../js/fancybox/jquery.fancybox.js',
'../js/fancybox/jquery.fancybox.pack.js',
'../js/fancybox/jquery.mousewheel-3.0.6.pack.js',
'../js/libs/jquery-1.7.1.min.js',
'../oncology/index.html',
'../oncology/manifest.json',
'../oncology/page1.html',
'../oncology/page10.html',
'../oncology/page11.html',
'../oncology/page12.html',
'../oncology/page13.html',
'../oncology/page14.html',
'../oncology/page15.html',
'../oncology/page16.html',
'../oncology/page17.html',
'../oncology/page18.html',
'../oncology/page19.html',
'../oncology/page2.html',
'../oncology/page20.html',
'../oncology/page21.html',
'../oncology/page22.html',
'../oncology/page23.html',
'../oncology/page24.html',
'../oncology/page25.html',
'../oncology/page26.html',
'../oncology/page27.html',
'../oncology/page28.html',
'../oncology/page29.html',
'../oncology/page3.html',
'../oncology/page30.html',
'../oncology/page31.html',
'../oncology/page32.html',
'../oncology/page33.html',
'../oncology/page34.html',
'../oncology/page35.html',
'../oncology/page36.html',
'../oncology/page37.html',
'../oncology/page38.html',
'../oncology/page39.html',
'../oncology/page4.html',
'../oncology/page40.html',
'../oncology/page41.html',
'../oncology/page42.html',
'../oncology/page43.html',
'../oncology/page44.html',
'../oncology/page45.html',
'../oncology/page46.html',
'../oncology/page47.html',
'../oncology/page48.html',
'../oncology/page49.html',
'../oncology/page5.html',
'../oncology/page50.html',
'../oncology/page51.html',
'../oncology/page52.html',
'../oncology/page53.html',
'../oncology/page54.html',
'../oncology/page55.html',
'../oncology/page56.html',
'../oncology/page57.html',
'../oncology/page58.html',
'../oncology/page6.html',
'../oncology/page7.html',
'../oncology/page8.html',
'../oncology/page9.html',
'../oncology/page64.html',
'../oncology/page65.html',
'../oncology/page66.html',
'../oncology/page67.html',
'../oncology/page68.html',
'../oncology/page69.html',
'../oncology/page70.html',

'../oncology/page77.html',
'../oncology/page78.html',
'../oncology/page79.html',
'../oncology/page80.html',
'../oncology/page181.html',
'../oncology/page81.html',
'../oncology/page82.html',
'../oncology/page83.html',
'../oncology/page84.html',

"../oncology/page88.html",
"../oncology/page89.html",
"../oncology/page90.html",

"../oncology/page93.html",
"../oncology/page94.html",
"../oncology/page95.html",
"../oncology/page96.html",
"../oncology/page97.html",
"../oncology/page98.html",
"../oncology/page99.html",
"../oncology/page100.html",
"../oncology/page101.html",
"../oncology/page102.html",
"../oncology/page103.html",
"../oncology/page104.html",
"../oncology/page105.html",
"../oncology/page106.html",
"../oncology/page107.html",
"../oncology/page108.html",
"../oncology/page109.html",
"../oncology/page110.html",
"../oncology/page111.html",
"../oncology/page112.html",
"../oncology/page113.html",
'../oncology/page200.html',
'../oncology/page201.html',
'../oncology/page202.html',
'../oncology/page203.html',
'../oncology/page204.html',
'../oncology/page205.html',
'../oncology/page206.html',
'../oncology/page207.html',
'../oncology/page208.html',
'../oncology/page209.html',
'../oncology/page210.html',
'../oncology/page211.html',
'../oncology/page212.html',
'../oncology/page213.html',
'../oncology/page214.html',
'../oncology/page215.html',
'../oncology/page216.html',
'../oncology/page217.html',
'../oncology/page218.html',
'../oncology/page219.html',
'../oncology/page220.html',
'../oncology/page221.html',
'../oncology/page222.html',
'../oncology/page223.html',
'../oncology/page224.html',
'../oncology/page225.html',
'../oncology/page226.html',
'../oncology/page227.html',
'../oncology/page228.html',
'../oncology/page229.html',
'../oncology/page230.html',
'../oncology/page231.html',
'../oncology/page232.html',
'../oncology/page233.html',
'../oncology/page234.html',
'../oncology/page235.html',
'../oncology/page236.html',
'../oncology/page237.html',
'../oncology/page238.html',
'../oncology/page239.html',
'../oncology/page240.html',
'../oncology/page241.html',
'../oncology/page242.html',
'../oncology/page243.html',
'../oncology/page244.html',
'../oncology/page245.html',
'../oncology/page246.html',
'../oncology/page247.html',
'../oncology/page248.html',
'../oncology/page249.html',
'../oncology/page250.html',
'../oncology/page251.html',

'../oncology/pwabuilder-sw.js',
'../oncology/icons/icon-128x128.png',
'../oncology/icons/icon-144x144.png',
'../oncology/icons/icon-152x152.png',
'../oncology/icons/icon-192x192.png',
'../oncology/icons/icon-512x512.png',
'../oncology/icons/icon-72x72.png',
'../oncology/icons/icon-96x96.png'
];

self.addEventListener("install", function (event) {
  console.log("[PWA Builder] Install Event processing");

  console.log("[PWA Builder] Skip waiting on install");
  self.skipWaiting();

  event.waitUntil(
    caches.open(CACHE).then(function (cache) {
      console.log("[PWA Builder] Caching pages during install");
      return cache.addAll(precacheFiles);
    })
  );
});

// Allow sw to control of current page
self.addEventListener("activate", function (event) {
  console.log("[PWA Builder] Claiming clients for current page");
  event.waitUntil(self.clients.claim());
});

// If any fetch fails, it will look for the request in the cache and serve it from there first
self.addEventListener("fetch", function (event) {
  if (event.request.method !== "GET") return;

  event.respondWith(
    fromCache(event.request).then(
      function (response) {
        // The response was found in the cache so we responde with it and update the entry

        // This is where we call the server to get the newest version of the
        // file to use the next time we show view
        event.waitUntil(
          fetch(event.request).then(function (response) {
            return updateCache(event.request, response);
          })
        );

        return response;
      },
      function () {
        // The response was not found in the cache so we look for it on the server
        return fetch(event.request)
          .then(function (response) {
            // If request was success, add or update it in the cache
            event.waitUntil(updateCache(event.request, response.clone()));

            return response;
          })
          .catch(function (error) {
            console.log("[PWA Builder] Network request failed and no cache." + error);
          });
      }
    )
  );
});

function fromCache(request) {

  headersLog = [];
  for (var pair of request.headers.entries()) {
    console.log(pair[0] + ': ' + pair[1]);
    headersLog.push(pair[0] + ': ' + pair[1])
  }
  console.log('Handling fetch event for', request.url, JSON.stringify(headersLog));

  if (request.headers.get('range')) {
    console.log('Range request for', request.url);
    var rangeHeader = request.headers.get('range');
    var rangeMatch = rangeHeader.match(/^bytes\=(\d+)\-(\d+)?/)
    var pos = Number(rangeMatch[1]);
    var pos2 = rangeMatch[2];
    if (pos2) {
      pos2 = Number(pos2);
    }

    console.log('Range request for ' + request.url, 'Range: ' + rangeHeader, "Parsed as: " + pos + "-" + pos2);

    return caches.open(CACHE)
      .then(function (cache) {
        return cache.match(request.url);
      }).then(function (res) {
        if (!res) {
          console.log("Not found in cache - doing fetch")
          return fetch(request)
            .then(res => {
              console.log("Fetch done - returning response ", res)
              return res.arrayBuffer();
            });
        }
        console.log("FOUND in cache - doing fetch")
        return res.arrayBuffer();
      }).then(function (ab) {
        console.log("Response procssing")
        let responseHeaders = {
          status: 206,
          statusText: 'Partial Content',
          headers: [
            ['Content-Type', 'video/mp4'],
            ['Content-Range', 'bytes ' + pos + '-' +
              (pos2 || (ab.byteLength - 1)) + '/' + ab.byteLength
            ]
          ]
        };

        console.log("Response: ", JSON.stringify(responseHeaders))
        var abSliced = {};
        if (pos2 > 0) {
          abSliced = ab.slice(pos, pos2 + 1);
        } else {
          abSliced = ab.slice(pos);
        }

        console.log("Response length: ", abSliced.byteLength)
        return new Response(
          abSliced, responseHeaders
        );
      })

  }



  // Check to see if you have it in the cache
  // Return response
  // If not in the cache, then return
  return caches.open(CACHE).then(function (cache) {
    return cache.match(request).then(function (matching) {
      if (!matching || matching.status === 404) {
        return Promise.reject("no-match");
      }

      return matching;
    });
  });
}

function updateCache(request, response) {
  return caches.open(CACHE).then(function (cache) {
    return cache.put(request, response);
  });
}