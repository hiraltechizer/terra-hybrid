(function($){
    var list_page = [];
    var nav_menu_items = [];
    $('.pages').on('change', function() {
        var pages = $(this).attr('data-pages');
        console.log('pages',pages)
        var _txt = $(this).parent().text();
        if( $(this).is(':checked') ) {
            nav_menu_items.push({ 
                'text': $(this).parent().text(), 
                'values': pages.split(',').reverse().pop()
            });
            
            $.merge(list_page, pages.split(','));
        } else {
            list_page = list_page.filter(function(el) {
                return pages.split(',').indexOf(el) < 0;
            });

            nav_menu_items.filter(function(el, i) {
                if(el.text === _txt) {
                    delete nav_menu_items[i];
                }
            });

            nav_menu_items = nav_menu_items.filter(function(el, i) {
                if(el) {
                    return el;
                }
            });
        }
    });

    $('#start-btn').prop('disabled', true);
    $('.pages').on('change', function () {
        if(list_page.length > 0) {
            $('#start-btn').prop('disabled',false);
        } else {
            $('#start-btn').prop('disabled', true);
        }
    });    


    $('#start-btn').on('click', function() {
        localStorage.clear();
        if(list_page) {
            localStorage.setItem('page-pref','1,'+ list_page);
            localStorage.setItem('nav-menu-item', JSON.stringify(nav_menu_items));
            localStorage.setItem('index',0);
            page_pref = localStorage.getItem('page-pref').split(',');
            window.location = '../oncology/page'+page_pref[localStorage.getItem('index')]+'.html';
        }
    });
})(jQuery)