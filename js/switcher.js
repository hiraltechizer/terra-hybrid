(function($){
    if(!localStorage.getItem('page-pref')) {
        window.location = '../oncology/page/index.html';
    }

    var page = $('meta[name="page"]').attr('content');

    if(!page) {
        window.location = '../oncology/page/index.html';
    }

    page_pref = localStorage.getItem('page-pref').split(',');
    nav_menu_items = JSON.parse(localStorage.getItem('nav-menu-item'));
    page_index = page_pref.indexOf(page);

    var getRightLink = function(page_index){
        page_next = page_index + 1;
        if(page_pref.length > parseInt(page_next)) {
            $('.link-btn-next').css("display", "block");
            return `../oncology/page${page_pref[page_next]}.html`;
        } else {
            $('.link-btn-next').css("display", "none");
            return '#';
        }
        
    };
    var getLeftLink = function(page_index){
        page_prev = page_index - 1;
        if(page_prev < 0 ) {
            $('.link-btn-prev').css("display", "none");
            return '#';
        } else {
            $('.link-btn-prev').css("display", "block");
            return `../oncology/page${page_pref[page_prev]}.html`;
        }
        
    };


    if($('ul.navbar-nav').length) {
        $.each(nav_menu_items, function(i, val){
            $('ul.navbar-nav').append(
                $('<li />').attr('class', 'nav-item').append(
                    $('<a />')
                        .attr('class', 'nav-link')
                        .attr('href', `../oncology/page${val.values}.html`)
                        .text(val.text)
                )
            )
        });
    }
    
    page_prev_link = getLeftLink(page_index);
    page_next_link = getRightLink(page_index);

    $('.link-btn-prev').parent().attr('href', page_prev_link);
    $('.link-btn-next').parent().attr('href', page_next_link);

    $("body").swipe({
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
            if (direction == "right") {
                //Previous
                window.open(page_prev_link, "_top");
            }
            else if (direction == "left") {
                //Next
                window.open(page_next_link, "_top");
            }

        },
        threshold: 100,
        allowPageScroll: 'auto'
    });
})(jQuery)